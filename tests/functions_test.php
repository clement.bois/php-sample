<?php

use PHPUnit\Framework\TestCase;

class functions_test extends TestCase
{
    public function test_kilometers_to_meters_ok()
    {
        require_once(dirname(__FILE__).'/../src/includes/functions.php');
        $this->assertEquals(1000, convert_length(1, "kilometers", "meters"));
    }

    public function test_kilometers_to_liters_ko()
    {
        require_once(dirname(__FILE__).'/../src/includes/functions.php');
        $this->assertEquals('Unsupported unit.', convert_length(1, "kilometers", "liters"));
    }

    public function test_celsius_to_fahrenheit_ok()
    {
        require_once(dirname(__FILE__).'/../src/includes/functions.php');
        $this->assertEquals(32, convert_temperature(0, "celsius", "fahrenheit"));
    }

    public function test_celsius_to_liters_ko()
    {
        require_once(dirname(__FILE__).'/../src/includes/functions.php');
        $this->assertEquals('Unsupported unit.', convert_temperature(0, "celsius", "liters"));
    }
}
