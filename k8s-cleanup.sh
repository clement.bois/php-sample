#!/bin/sh

set -e

echo "[measure-convert] cleanup script: delete all objects with label app=$environment_name"
kubectl delete all,pvc,secret,ingress -l "app=$environment_name"
