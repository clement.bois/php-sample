FROM registry.hub.docker.com/library/php:8.3-apache

RUN apt-get -y update && apt-get -y upgrade \
    &&  rm -rf /var/lib/apt/lists/*

COPY src/ /var/www/html/
RUN sed -i "s/Listen 80/Listen 8080/" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf
EXPOSE 8080
